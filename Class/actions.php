<?php
/**
 * Created by PhpStorm.
 * User: Özlem
 * Date: 16.04.2017
 * Time: 22:45
 */

class actions
{

    function genel($name,$type,$color,$voice)
    {
        echo "$name is in $type. <br>
               Color is $color. <br>
               Voice is $voice.<br>";
    }

    function read($name){
        echo "$name can read. <br>";
    }

    function eat($name){
        echo "$name can eat. <br>";
    }

    function run($name){
        echo "$name can run. <br>";
    }

    function fly($name){
        echo "$name can fly. <br>";
    }

    function swim($name){
        echo "$name can swim. <br>";
    }

    function bite($name){
        echo "$name can bite. <br>";
    }

    function grovel($name){
        echo "$name can grovel. <br>";
    }
}