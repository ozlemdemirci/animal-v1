<?php
/**
 * Created by PhpStorm.
 * User: Özlem
 * Date: 16.04.2017
 * Time: 18:13
 */

class Birds extends actions
{
    function __construct($name,$type,$color,$voice)
    {
        $this->genel($name,$type,$color,$voice);
        $this->fly($name);
        $this->eat($name);
    }
}