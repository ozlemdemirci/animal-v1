<?php
/**
 * Created by PhpStorm.
 * User: Özlem
 * Date: 16.04.2017
 * Time: 18:14
 */

class Reptiles extends actions
{
    function __construct($name,$type,$color,$voice)
    {
        $this->genel($name,$type,$color,$voice);
        $this->bite($name);
        $this->eat($name);
        $this->grovel($name);
    }
}